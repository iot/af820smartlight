/*

Project: AF820 Smart Light
Author: Glenn Schmottlach

The MIT License (MIT)

Copyright (c) 2016 Glenn Schmottlach

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

'use strict';

var ip            = require('ip');
var ipmask        = require('ipmask')();
var uuid          = require('node-uuid');
var util          = require('util');
var EventEmitter  = require('events');
var dgram         = require('dgram');
var log4js        = require('log4js');
var format        = require('string-format');

// Remote registration service IP and port
var REMOTE_REGISTRATION_ADDR = '198.199.115.33';
var REMOTE_REGISTRATION_PORT = 30001;

// Define the default port the AF820 listens on
var DEFAULT_AF820_LOCAL_PORT = 5880;

// Minimum size packet (bytes
var MIN_PACKET_LEN = 37;

// The length of a status packet
var STATUS_PACKET_LEN = MIN_PACKET_LEN + 16;

// The length of a register response packet
var REGISTER_RESP_PACKET_LEN = MIN_PACKET_LEN + 6;


// Default timeout (msec) to wait for a response to a request
var DEFAULT_RESPONSE_TIMEOUT = 1000;

// Default number of retry attempts
var DEFAULT_NUM_RETRIES = 2;

// Smart Light device code
var DEV_CODE_AF820  = 0x20;

// Header bytes for every packet
var PKT_HDR_FIRST = 0x55;
var PKT_HDR_SECOND_LOCAL = 0xAA;
var PKT_HDR_SECOND_REMOTE = 0x66;

// Offset into field of basic packet
var PKT_HDR_OFFSET = 0;
var PKT_FIXED_OFFSET = 4;
var PKT_LEN_OFFSET = 2;
var DEV_MAC_OFFSET = 7;
var DEV_CODE_OFFSET = 18;
var DEV_VER_OFFSET = 19;
var DEV_FUNC_OFFSET = 20;
var CTRL_DEV_ID_OFFSET = 23;
var SEQ_NUM_OFFSET = 31;
var EXTRA_DATA_LEN_OFFSET = 33;
var EXTRA_DATA_START_OFFSET = 37;

//
// Device Function Identifiers
//

// These functions return check packet responses (FUNC_ID_CHECK_RESP)
var FUNC_ID_TURN_LIGHT_ON_REQ = 0x01;
var FUNC_ID_TURN_LIGHT_OFF_REQ = 0x02;
var FUNC_ID_SET_COLOR_REQ = 0x03;
var FUNC_ID_RANDOM_REQ = 0x04;
var FUNC_ID_SLEEP_REQ = 0x05;
var FUNC_ID_CHECK_REQ = 0xFF;


// These return packets specific to the request

var FUNC_ID_WIFI_CONFIG_REQ = 0xF0;
var FUNC_ID_WIFI_CONFIG_CHECK_REQ = 0x01;
var FUNC_ID_WIFI_CONFIG_SET_REQ = 0x03;
var FUNC_ID_WIFI_CONFIG_RESTART_REQ = 0x05;
var FUNC_ID_RESET_REQ = 0xF7;
var FUNC_ID_REGISTER_REQ = 0xFB;

// Response messages
var FUNC_ID_CHECK_RESP = 0xFE;
var FUNC_ID_REGISTER_RESP = 0xFA;
var FUNC_ID_WIFI_CONFIG_RESP = 0xF0;



var AF820LightController = function(ipAddr, port, logger) {

  if ( !(this instanceof AF820LightController) ) {
    return new AF820LightController(ipAddr, port, logger);
  }

  if ( !logger ) {
    this._logger = log4js.getLogger('af820');
  } else {
    this._logger = logger;
  }

  this._ipAddr = ipAddr;
  if ( !ip.isV4Format(this._ipAddr) ) {
    this._ipAddr = ip.subnet(ipmask.address, ipmask.netmask).broadcastAddress;
  }

  this._port = port;
  if ( typeof(port) !== 'number' ) {
    this._port = DEFAULT_AF820_LOCAL_PORT;
  }

  if ( ip.subnet(this._ipAddr, ipmask.netmask).broadcastAddress === this._ipAddr ) {
    this._isBroadcastAddr = true;
  } else {
    this._isBroadcastAddr = false;
  }

  // A fake controlling device unique identifier
  this._ctrlDevId = new Buffer(uuid.v1().slice(0,8), 'hex');
  this._logger.debug('ctrlDevId: ' + this._ctrlDevId.toString('hex'));

  this._server = null;
  this._pendingReq = {};
  this._seqCount = 0;
};
util.inherits(AF820LightController, EventEmitter);


var method = AF820LightController.prototype;

method._getSeqNum = function() {
  this._seqCount = (this._seqCount + 1) % 65536;
  if ( 0 === this._seqCount ) {
    ++this._seqCount;
  }

  return this._seqCount;
};

method._decodeResponse = function(data) {
  var pkt;
  var extraDataLen;
  var ssidLen;
  var ssidIdx;
  var pwdLen;
  var pwdIdx;
  
  if ( !Buffer.isBuffer(data)) {
    return null;
  }
  
  if ( data.length < MIN_PACKET_LEN ) {
    return null;
  }

  if ( !((data[0] === PKT_HDR_FIRST) && ((data[1] === PKT_HDR_SECOND_LOCAL) ||
      (data[1] === PKT_HDR_SECOND_REMOTE))) ) {
    return null;
  }


  var devMac = [];
  var macBuf = data.slice(DEV_MAC_OFFSET, DEV_MAC_OFFSET+8);
  for ( var idx = 0; idx < macBuf.length; ++idx ) {
    devMac.push(macBuf[idx]);
  }

  pkt = {
    // Basic fields
    devMac : devMac,
    ctrlDevId: data.readUInt32BE(CTRL_DEV_ID_OFFSET),
    devFuncId: data[DEV_FUNC_OFFSET],
    devVer: data[DEV_VER_OFFSET],
    devCode: data[DEV_CODE_OFFSET],
    seqNum: data.readUInt16BE(SEQ_NUM_OFFSET),
    extraDataLen: data.readUInt16BE(EXTRA_DATA_LEN_OFFSET),
  };

  if ( (pkt.devFuncId === FUNC_ID_CHECK_RESP) &&
      (data.length === STATUS_PACKET_LEN) ) {
    // Decode the "extra" data fields
    pkt.lightOn = data[EXTRA_DATA_START_OFFSET] !== 0;
    pkt.random = data[EXTRA_DATA_START_OFFSET + 1] !== 0;
    pkt.red = data.readUInt16BE(EXTRA_DATA_START_OFFSET + 2);
    pkt.green = data.readUInt16BE(EXTRA_DATA_START_OFFSET + 4);
    pkt.blue = data.readUInt16BE(EXTRA_DATA_START_OFFSET + 6);
    pkt.white = data.readUInt16BE(EXTRA_DATA_START_OFFSET + 8);
    pkt.time = data.readUInt16BE(EXTRA_DATA_START_OFFSET + 10);
    pkt.lux = data.readUInt16BE(EXTRA_DATA_START_OFFSET + 12);
    pkt.sleepTime = data.readUInt16BE(EXTRA_DATA_START_OFFSET + 14);
  }
  else if ( (pkt.devFuncId === FUNC_ID_REGISTER_RESP) &&
      (data.length === REGISTER_RESP_PACKET_LEN) ) {
    pkt.ipAddr = format('{}.{}.{}.{}', data[EXTRA_DATA_START_OFFSET],
                                    data[EXTRA_DATA_START_OFFSET+1],
                                    data[EXTRA_DATA_START_OFFSET+2],
                                    data[EXTRA_DATA_START_OFFSET+3]);
    pkt.port = data.readUInt16BE(EXTRA_DATA_START_OFFSET+4);     
  }
  else if ( (pkt.devFuncId === FUNC_ID_WIFI_CONFIG_RESP) && (pkt.extraDataLen >= 3) ) {
    ssidIdx = EXTRA_DATA_START_OFFSET + 2;
    ssidLen = data[EXTRA_DATA_START_OFFSET + 1];
    if ( typeof ssidLen === 'number' ) {
      pkt.ssid = data.slice(ssidIdx, ssidIdx + ssidLen).toString();
      pwdIdx = ssidIdx + ssidLen + 1;
      pwdLen = data[ssidIdx + ssidLen];
      if ( typeof pwdLen === 'number' ) {
        pkt.pwd = data.slice(pwdIdx, pwdIdx + pwdLen).toString();
      }
    }
  } else {
    pkt.extraData = data.slice(EXTRA_DATA_START_OFFSET).toJSON().data;
  }

  return pkt;
};


method._onReceivePacket = function(pkt, rinfo) {
  var response;
  var req;
  this._logger.debug('Received data: ' + pkt.toString('hex') +
                    ' from ' + rinfo.address + ':' + rinfo.port);
  response = this._decodeResponse(pkt);
  if ( response ) {
    req = this._pendingReq[response.seqNum];
    if ( req ) {
      clearTimeout(req.timeout);
      if ( typeof req.pkt.callback === 'function' ) {
        req.pkt.callback(undefined, response);
      }
      delete this._pendingReq[response.seqNum];
    }
  }
};


method._sendPacket = function(pkt, ipAddr, port, isRemote) {

  var self = this;
  var rawPkt = self._createPacket(isRemote, DEV_CODE_AF820, 0,
                                  pkt.funcId, pkt.devMac,
                                  pkt.seqNum, pkt.data);

  var destPort = port || self._port;
  var destIpAddr = ipAddr || self._ipAddr;
  if ( typeof(pkt.nRetries) !== 'number' ) {
    pkt.nRetries = 0;
  }

  function onTimeout() {
    var newSeqNum;

    // Remove the current pending entry
    delete self._pendingReq[pkt.seqNum];
    
    if ( pkt.nRetries > 0 ) {
      pkt.nRetries--;
      // Generate a new sequence number for the packet
      newSeqNum = self._getSeqNum();
      self._logger.debug('Resending pkt(#' + pkt.seqNum +
                        ') as pkt(#' + newSeqNum + ')');
      pkt.seqNum = newSeqNum;
      if ( !self._server ) {
        pkt.callback(new Error('Disconnected from server'));
      }
      else {
        self._server.send(rawPkt, 0, rawPkt.length,
            destPort, destIpAddr, function(err) {
          // Called when the message has been sent.
          if ( err ) {
            pkt.callback(err);
          } else {
            // Reset the timeout and mark the request as pending
            self._pendingReq[pkt.seqNum] = {
                                timeout : setTimeout(onTimeout,
                                                    pkt.timeout),
                                pkt : pkt
                                };
          }
        });
      }
    }
    else {
      pkt.callback(new Error('Timeout'));
    }
  }

  if ( !this._server ) {
    throw new Error('Service not started');
  } else {
    self._logger.debug('Send data:' + util.inspect(rawPkt));
    self._server.send(rawPkt, 0, rawPkt.length, destPort, destIpAddr,
                    function(err) {
      
      if ( err ) {
        self.logger.error('Failed sending packet: ' + err);
      }
      if ( typeof(pkt.callback) === 'function' ) {
        // Called when the message has been sent.
        if ( err ) {
          pkt.callback(err);
        } else {         
          self._pendingReq[pkt.seqNum] = {
                                         timeout : setTimeout(onTimeout,
                                                   pkt.timeout),
                                         pkt : pkt
                                         };
        }
      }
    });
  }
};


method._createPacket = function(isRemote, devCode, devVersion, devFuncId, devMac, seqNum, data) {
  var macData = null;
  
  function checkByteValue(val, parmName) {
    if ( typeof(val) !== 'number' ) {
      throw new TypeError('Expected a number for ' + parmName);
    }
    
    if ( (val < 0) || (val > 255) ) {
      throw new RangeError(parmName + ' is out range [0,255]');
    }
  }
  
  if ( Array.isArray(devMac)) {
    macData = new Buffer(devMac);
  } else if ( Buffer.isBuffer(devMac) ) {
    macData = devMac;
  } else {
    if ( typeof(devMac) !== 'string' ) {
      throw new TypeError('Expected device MAC as 8-byte hex string');
    }
    devMac = devMac.replace(/ /g, '').replace(/:/g, '').replace('0x', '');
    if ( devMac.length < 16 ) {
      devMac = devMac.concat('0'.repeat(16 - devMac.length));
    }
    macData = new Buffer(devMac, 'hex');
  }

  if ( macData.length !== 8 ) {
    throw new RangeError('Device MAC must contain 8 bytes');
  }

  checkByteValue(devCode, 'devCode');
  checkByteValue(devVersion, 'devVersion');
  checkByteValue(devFuncId, 'devFuncId');
  
  if ( typeof(seqNum) !== 'number' ) {
    throw new TypeError('Expected a sequence number');
  }

  var extraData = null;
  if ( data ) {
    if ( typeof(data) === 'string' ) {
      extraData = new Buffer(data, 'hex');
    } else if ( Array.isArray(data) ) {
      extraData = new Buffer(data);
    } else if ( Buffer.isBuffer(data) ) {
      extraData = data;
    } else {
      throw new TypeError('data expect to be a hex string, byte array, or buffer');
    }
  }

  var pktLen = MIN_PACKET_LEN;
  if ( extraData ) {
    pktLen += extraData.length;
  }

  // Convert the device MAC to a byte buffer
  var pktBuf = new Buffer.alloc(pktLen, 0);
  pktBuf[PKT_HDR_OFFSET] = PKT_HDR_FIRST;
  if ( isRemote ) {
    pktBuf[PKT_HDR_OFFSET+1] = PKT_HDR_SECOND_REMOTE;    
  }
  else {
    pktBuf[PKT_HDR_OFFSET+1] = PKT_HDR_SECOND_LOCAL;
  }
  pktBuf.writeUInt16BE(pktLen, PKT_LEN_OFFSET);
  pktBuf[PKT_FIXED_OFFSET] = 0x01;
  macData.copy(pktBuf, DEV_MAC_OFFSET);
  pktBuf[DEV_CODE_OFFSET] = devCode;
  pktBuf[DEV_VER_OFFSET] = devVersion;
  pktBuf[DEV_FUNC_OFFSET] = devFuncId;
  this._ctrlDevId.copy(pktBuf, CTRL_DEV_ID_OFFSET);
  pktBuf.writeUInt16BE(seqNum, SEQ_NUM_OFFSET);
  if ( extraData ) {
    pktBuf.writeUInt16BE(extraData.length, EXTRA_DATA_LEN_OFFSET);
    extraData.copy(pktBuf, EXTRA_DATA_START_OFFSET);
  } 

  return pktBuf; 
};

method.start = function (callback) {
  var self = this;
  
  var stopServer = function() {
    return new Promise(function(resolve, reject) {
      if ( self._server ) {
        self.stop(function(err) {
          if ( err ) {
            reject(err);
          } else {
            resolve();
          }
        });
      } else {
        resolve();
      }
    });
  };

  var startServer = function() {
    return new Promise(function(resolve, reject) {
      self._server = dgram.createSocket('udp4', function(pkt, rinfo) {
        self._onReceivePacket(pkt, rinfo);
      });
      self._server.bind(function() {
        if ( self._isBroadcastAddr ) {
          self._server.setBroadcast(true);
        }
        resolve();
      });
    });
  };

  return stopServer()
    .then(startServer)
    .then(function() {
      if ( typeof(callback) === 'function' ) {
        callback();
      }
      self.emit('listening');
    })
    .catch( function(e) {
      if ( typeof(callback) === 'function' ) {
        callback(e);
      }      
    });  
};

method.stop = function(callback) {
  var self = this;
  
  if ( self._server ) {
    self._server.close(function() {
      self._server = null;
      if ( typeof(callback) === 'function' ) {
        callback();
      }
    });
  } else {
    if ( typeof(callback) === 'function' ) {
      process.nextTick(callback);
    }
  }
};


method.turnOn = function(devMac, callback, timeout, nRetries) {
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_TURN_LIGHT_ON_REQ,
      devMac : devMac,
      data : null
  };
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.turnOff = function(devMac, callback, timeout, nRetries) {
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_TURN_LIGHT_OFF_REQ,
      devMac : devMac,
      data : null
  };
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.setRandom = function (devMac, enable, callback, timeout, nRetries) {
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_RANDOM_REQ,
      devMac : devMac,
      data : new Buffer.alloc(1, 0)
  };
  if ( enable ) {
    pkt.data[0] = 1;
  }
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.setColor = function(devMac, red, green, blue, white, time,
                          lux, callback, timeout, nRetries) {
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_SET_COLOR_REQ,
      devMac : devMac,
      data : new Buffer.alloc(12, 0)
  };
  
  pkt.data.writeUInt16BE(red, 0);
  pkt.data.writeUInt16BE(green, 2);
  pkt.data.writeUInt16BE(blue, 4);
  pkt.data.writeUInt16BE(white, 6);
  pkt.data.writeUInt16BE(time, 8);
  pkt.data.writeUInt16BE(lux, 10);
  
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.sleep = function(devMac, time, callback, timeout, nRetries) {
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_SLEEP_REQ,
      devMac : devMac,
      data : new Buffer.alloc(2, 0)
  };
  pkt.data.writeUInt16BE(time, 0);
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.checkStatus = function(devMac, callback, timeout, nRetries) {
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_CHECK_REQ,
      devMac : devMac,
      data : null
  };
  this._sendPacket(pkt); 
  return pkt.seqNum;
};

method.checkWifi = function(devMac, callback, timeout, nRetries) {
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_WIFI_CONFIG_REQ,
      devMac : devMac,
      data : new Buffer([FUNC_ID_WIFI_CONFIG_CHECK_REQ])
  };
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.setWifi = function(devMac, ssid, password, callback, timeout, nRetries) {
  var data = new Buffer.alloc(3 + ssid.length + password.length);
  data[0] = FUNC_ID_WIFI_CONFIG_SET_REQ;
  data[1] = ssid.length;
  data.write(ssid, 2);
  data[2 + ssid.length] = password.length;
  data.write(password, 3 + ssid.length);
  
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_WIFI_CONFIG_REQ,
      devMac : devMac,
      data : data
  };
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.restartWifi = function(devMac) { 
  var pkt = {
      callback : null,
      seqNum : this._getSeqNum(),
      timeout : DEFAULT_RESPONSE_TIMEOUT,
      nRetries: 0,
      funcId : FUNC_ID_WIFI_CONFIG_REQ,
      devMac : devMac,
      data : new Buffer([FUNC_ID_WIFI_CONFIG_RESTART_REQ])
  };
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.reset = function(devMac, callback, timeout, nRetries) { 
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_RESET_REQ,
      devMac : devMac,
      data : null
  };
  this._sendPacket(pkt);
  return pkt.seqNum;
};

method.register = function(devMac, callback, timeout, nRetries) { 
  var pkt = {
      callback : callback,
      seqNum : this._getSeqNum(),
      timeout : timeout || DEFAULT_RESPONSE_TIMEOUT,
      nRetries: nRetries || DEFAULT_NUM_RETRIES,
      funcId : FUNC_ID_REGISTER_REQ,
      devMac : devMac,
      data : null
  };
  this._sendPacket(pkt, REMOTE_REGISTRATION_ADDR, REMOTE_REGISTRATION_PORT, true);
  return pkt.seqNum;
};

module.exports = AF820LightController;


