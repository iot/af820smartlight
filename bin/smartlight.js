#!/usr/bin/env node
/*

Project: AF820 Smart Light
Author: Glenn Schmottlach

The MIT License (MIT)

Copyright (c) 2016 Glenn Schmottlach

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

'use strict';

var yargs                 = require('yargs');
var fs                    = require('fs');
var mqtt                  = require('mqtt');
var log4js                = require('log4js');
var util                  = require('util');
var path                  = require('path');
var format                = require('string-format');
var AF820LightController  = require('../lib/af820');

var appLogger = log4js.getLogger('smartlight');
var ramp = null;

function jsonClone(arg) {
  return JSON.parse(JSON.stringify(arg));
}

function toBoolean(value) {
  switch( value.toString().toLowerCase().trim() ) {
    case "true":
    case "t":
    case "yes":
    case "y":
    case "1":
      return true;
    case "false":
    case "f":
    case "no":
    case "n":
    case "0":
    case null:
      return false;
  default:
      return Boolean(value);
  }
}

function toNumber(value, defValue) {
  var num = parseInt(value, 0);
  if ( isNaN(num) ) {
    return defValue;
  }
  return num;
}

function toDevice(arg) {
  var macData;
  var devMac = arg.replace(/ /g, '').replace(/:/g, '').replace('0x', '');
  if ( devMac.length < 16 ) {
    devMac = devMac.concat('0'.repeat(16 - devMac.length));
  }
  macData = new Buffer(devMac, 'hex');
  if ( macData.length !== 8 ) {
    throw new RangeError('Device MAC must contain 8 bytes');
  }
  return macData;
}


function respHandler(resolve, reject, opt, req, err, statusPkt) {
  var resp;
  if ( err ) {
      reject(req.cmd + ',ERROR,' + err.toString());
  }
  else {
    if ( opt.verbose ) {
      appLogger.debug('Response:' + util.inspect(statusPkt));
    }

    switch(req.cmd) {
      case 'turnOn':
      case 'turnOff':
        resolve(req.cmd + ',OK' + ',lightOn=' + (statusPkt.lightOn ? 'true' : 'false'));
        break;

      case 'restartWifi':
      case 'register':
      case 'reset':
        resolve(req.cmd + ',OK');
        break;

      case 'setBrightness':
        resolve(req.cmd + ',OK,percent=' +
                Math.floor(((statusPkt.white / 255.0) * 100)));
        break;

      case 'checkWifi':
        resolve(req.cmd + ',OK,ssid=' + statusPkt.ssid + ',pwd=' + statusPkt.pwd);
        break;
        
      case 'setRandom':
        resolve(req.cmd + ',OK,random=' + (statusPkt.random ? 'true':'false'));
        break;

      case 'setColor':
        resolve(req.cmd + ',OK' +
                ',red=' + statusPkt.red.toString() +
                ',green=' + statusPkt.green.toString() +
                ',blue=' + statusPkt.blue.toString() +
                ',white=' + statusPkt.white.toString() +
                ',time=' + statusPkt.time.toString() +
                ',lux=' + statusPkt.lux.toString());
        break;

      case 'checkStatus':
        resolve(req.cmd + ',OK' +
          ',devMac=0x' + Buffer.from(statusPkt.devMac).toString('hex') +
          ',ctrlDevId=0x' + statusPkt.ctrlDevId.toString(16) +
          ',devFuncId=' + statusPkt.devFuncId.toString() +
          ',devVer=' + statusPkt.devVer.toString() +
          ',devCode='+ statusPkt.devCode.toString() +
          ',lightOn=' + (statusPkt.lightOn ? 'true' : 'false') +
          ',random=' + (statusPkt.random ? 'true' : 'false') +
          ',red=' + statusPkt.red.toString() +
          ',green=' + statusPkt.green.toString() +
          ',blue=' + statusPkt.blue.toString() +
          ',white=' + statusPkt.white.toString() +
          ',time=' + statusPkt.time.toString() +
          ',lux=' + statusPkt.lux.toString() +
          ',sleepTime=' + statusPkt.sleepTime.toString());
        break;
        
      case 'sleep':
        resolve(req.cmd + ',OK' + ',sleepTime=' + statusPkt.sleepTime.toString());
        break;

      case 'setWifi':
        resolve(req.cmd + ',OK');
        break;

      default:
        resolve(req.cmd + ',ERROR,Unexpected command');
        break;
    }
  }
}

//
// Class Ramp
//
function Ramp(smartLight, device) {
  this.smartLight = smartLight;
  this.device = device;
  this.duration = 15;   //min
  this.direction = 'up';
  this.timer = null;
  this.rampComplete = false;
  this.callback = null;
  this.curveIdx = 1;
  this.delta = 1;
  this.colorCurve = [
                   //R  G  B  W
                   [ 0, 0, 0, 0 ],
                   [ 6, 6, 0, 0 ],
                   [ 8, 8, 0, 0 ],
                   [ 10, 10, 0, 0 ],
                   [ 12, 12, 0, 0 ],
                   [ 15, 15, 0, 0 ],
                   [ 18, 18, 0, 0 ],
                   [ 20, 20, 0, 0 ],
                   [ 22, 22, 0, 0 ],
                   [ 25, 25, 0, 0 ],
                   [ 28, 28, 0, 0 ],
                   [ 30, 30, 0, 0 ],
                   [ 35, 35, 0, 0 ],
                   [ 40, 40, 0, 0 ],
                   [ 45, 45, 0, 0 ],
                   [ 50, 50, 0, 0 ],
                   [ 55, 55, 0, 0 ],
                   [ 60, 60, 0, 0 ],
                   [ 65, 65, 0, 0 ],
                   [ 70, 70, 0, 0 ],
                   [ 75, 75, 0, 0 ],
                   [ 80, 80, 0, 0 ],
                   [ 90, 90, 0, 0 ],
                   [ 110, 110, 0, 0 ],
                   [ 150, 150, 0, 0 ],
                   [ 180, 180, 0, 0 ],
                   [ 190, 190, 0, 0 ],
                   [ 210, 210, 0, 0 ],
                   [ 255, 255, 0, 0 ],
                   [ 0, 0, 0, 255 ],
                   ];
}

Ramp.prototype.start = function(dir, duration, callback) {
  var self = this;
  var period;
  var rgbw;
  var rampComplete = false;
  this.duration = ( typeof(duration) === 'number' ) ? duration : 15;
  this.direction = ( typeof(dir) === 'string' ) ? dir : 'up';

  if ( this.duration <= 0 ) {
    throw new RangeError('Duration in minutes must be > 0');
  }

  if ( (this.direction !== 'up') && (this.direction !== 'down') ) {
    throw new Error('Direction must be "up" or "down"');
  }

  if ( this.direction === 'up' ) {
    rgbw = this.colorCurve[0];
    this.delta = 1;
    this.curveIdx = 0;
  }
  // Else ramp down
  else {
    rgbw = this.colorCurve[this.colorCurve.length - 1];
    this.delta = -1;
    this.curveIdx = this.colorCurve.length - 1;
  }

  // Stop any outstanding action first
  this.stop();

  // Convert duration to milliseconds
  this.duration *= 60 * 1000;
  period = this.duration / (this.colorCurve.length - 1);
  this.callback = callback;

  function onResponse(err, statusPkt) {
    if ( err ) {
      appLogger.error('Failed to set color: ' + err);
    }
    
    if ( self.rampComplete ) {
      self.stop();
      if ( typeof(self.callback) === 'function' ) {
        self.callback();
        self.callback = null;
      }
    }
  }

  function doStep() {
    self.curveIdx += self.delta;

    if ( (self.curveIdx === 0) ||
      (self.curveIdx === self.colorCurve.length - 1) ) {
      self.rampComplete = true;
    }

    if ( (self.curveIdx >= 0) && (self.curveIdx < self.colorCurve.length) ) {
      rgbw = self.colorCurve[self.curveIdx];
      self.smartLight.setColor(self.device, rgbw[0], rgbw[1], rgbw[2],
                              rgbw[3], 20, 0, onResponse);
    }
  }

  /* Immediately execute the first step */
  doStep();
  this.timer = setInterval(doStep, period);
};

Ramp.prototype.stop = function() {
  if ( this.timer ) {
    clearInterval(this.timer);
    this.timer = null;
    if ( !this.rampComplete && (typeof this.callback === 'function') ) {
      this.callback(new Error('Ramp stopped prematurely'));
      this.callback = null;
    }
  }
};



function execCommand(smartLight, dev, opt, argv) {
  return new Promise(function(resolve, reject) {
    var cmdObj;
    var value;
    var disableRamp = true;
    
    function onResponse(err, statusPkt) {
      return respHandler(resolve, reject, opt, cmdObj, err, statusPkt);
    }
    
    //
    // Convert the command arguments into a object
    //

    cmdObj = { cmd:argv[0] };
    switch (cmdObj.cmd) {
      case 'turnOn':
      case 'turnOff':
      case 'reset':
      case 'restartWifi':
        break;

      case 'checkStatus':
      case 'checkWifi':
      case 'register':
        disableRamp = false;
        break;

      case 'ramp':
        cmdObj.direction = argv[1];
        cmdObj.duration = toNumber(argv[2], 15);
        break;

      case 'setRandom':
        cmdObj.enable = toBoolean(argv[1]);
        break;
      
      case 'setColor':
        cmdObj.red = toNumber(argv[1], 0);
        cmdObj.green = toNumber(argv[2], 0);
        cmdObj.blue = toNumber(argv[3], 0);
        cmdObj.white = toNumber(argv[4], 255);
        cmdObj.time = toNumber(argv[5], 0);
        cmdObj.lux = toNumber(argv[6], 50);
        break;
      
      case 'sleep':
        cmdObj.sleepTime = toNumber(argv[1], 0);
        break;
      
      case 'setWifi':
        cmdObj.ssid = argv[1];
        cmdObj.password = argv[2];
        break;
      
      case 'setBrightness':
        cmdObj.adjType = typeof argv[1] === 'string' ? argv[1] : 'abs';
        cmdObj.value = toNumber(argv[2], 0);
        cmdObj.time = toNumber(argv[3], 10);
        break;
        
      default:
        return reject(new Error('Unknown command'));
    }

    // Possibly disable any active ramp function
    if ( disableRamp && ramp ) {
      ramp.stop();
      ramp = null;
    }
    //
    // Now actually execute the command
    //
    switch ( cmdObj.cmd ) {
      case 'turnOn':
        smartLight.turnOn(dev, onResponse);
        break;
        
      case 'turnOff':
        smartLight.turnOff(dev, onResponse);
        break;

      case 'ramp':
        try {
          ramp = new Ramp(smartLight, dev);
          ramp.start(cmdObj.direction, cmdObj.duration, function(err) {
            if ( err ) {
              appLogger.error('Ramp error: ' + err);
            }
            else {
              appLogger.debug('Ramp complete');
            }
          });
          resolve(cmdObj.cmd + ',OK');
        }
        catch ( err ) {
          resolve(cmdObj.cmd + ',ERROR,' + err);
        }
        break;

      case 'setRandom':
        smartLight.setRandom(dev, cmdObj.enable || false,
                            onResponse);
        break;
        
      case 'setColor':
        smartLight.setColor(dev, cmdObj.red, cmdObj.green,
                              cmdObj.blue, cmdObj.white,
                              cmdObj.time, cmdObj.lux, onResponse);
        break;

      case 'sleep':
        smartLight.sleep(dev, cmdObj.sleepTime, onResponse);
        break;

      case 'setBrightness':
        smartLight.checkStatus(dev, function(err, resp) {
          if ( err ) {
            respHandler(resolve, reject, opt,
                        {cmd: 'setBrightness'}, err, resp);
          }
          else {
            // If this is an absolute adjustment
            if ( cmdObj.adjType.startsWith('abs') ) {
              value = Math.min(100, Math.max(0,cmdObj.value));
              value = (255 * (Math.abs(value) / 100.0)) | 0;
            }
            // Else relative
            else {
              value = Math.min(255, Math.max(0, resp.white +
                      (((cmdObj.value / 100.0) * 255.0) | 0)));
            }
            smartLight.setColor(dev, 0, 0, 0, value,
                              cmdObj.time, resp.lux, onResponse);
          }
        });
        break;
        
      case 'checkStatus':
        smartLight.checkStatus(dev, onResponse);
        break;

      case 'checkWifi':
        smartLight.checkWifi(dev, onResponse);
        break;

      case 'setWifi':
        smartLight.setWifi(dev, cmdObj.ssid, cmdObj.password, onResponse);
        break;

      case 'restartWifi':
        // This request has no response
        smartLight.restartWifi(dev);
        respHandler(resolve, reject, opt, cmdObj, null, {cmd:cmdObj.cmd});
        break;
    
      case 'reset':
        smartLight.reset(dev, onResponse);
        break;
    
      case 'register':
        smartLight.register(dev, onResponse);
        break;
      
      default:
        return reject(new Error('Unknown command'));
    }
  });
}


function SmartLightMqttClient(smartLight, argv) {

  if ( !(this instanceof SmartLightMqttClient) ) {
    return new SmartLightMqttClient(argv);
  }

  this._url = argv.cfg.mqtt.url;
  this._opts = {
      clientId:'smartlight_' + Math.random().toString(16).substr(2, 8),
      username: argv.cfg.mqtt.user,
      password: argv.cfg.mqtt.password,
      reconnectPeriod: 10 * 1000,
      will: {
        topic: argv.cfg.mqtt.topicWill,
        payload: 'offline',
        qos: 1,
        retain: true
      }
  };
  this._client = null;
  this._isConnected = false;
  this._verbose = argv.verbose;
  // Clone the bulbs
  this._bulbs = jsonClone(argv.cfg.bulbs);
  this._logger = log4js.getLogger('mqtt');
  this._smartLight = smartLight;

  return this;
}

SmartLightMqttClient.prototype._onMessage = function(topic, msg, pkt) {
  var self = this;
  var bulb;
  var req;
  var bulbName;
  var cmd;

  function publishResult(results) {
    return new Promise(function(resolve, reject) {
      if ( bulb.statusTopic ) {
        self._client.publish(bulb.statusTopic, results, {qos:2}, function() {
          self._logger.debug('status posted to: ' + bulb.statusTopic);
          resolve();
        });
      }
      else {
        self._logger.warn('no status topic defined for bulb ' + req[0]);
        resolve();
      }
    });
  }

  self._logger.debug(format('onMessage:\ntopic={}\nmsg={}\npkt={}',
                    topic, msg, util.inspect(pkt)));

  try {
    // Determine which bulb sent the command
    for ( var bulbName in self._bulbs ) {
      if ( self._bulbs.hasOwnProperty(bulbName) &&
          (typeof self._bulbs[bulbName].cmdTopic === 'string') &&
          ( self._bulbs[bulbName].cmdTopic === topic) ) {
        bulb = self._bulbs[bulbName];
        break;
      }
    }
    
    // Split the whitespace separated arguments
    req = msg.toString().trim().split(/\s+/);
    self._logger.debug('Parsed command: ' + util.inspect(req));
    cmd = req[1];

    if ( !bulb ) {
      self._logger.error('No matching bulb specified in command: ' + msg);
    } else {
      return execCommand(this._smartLight, toDevice(bulb.device),
                        {verbose: this._verbose}, req)
      .then(publishResult)
      .catch(function(reason) {
        self._logger.error(format('[{}/{}] failed: {}', bulbName, cmd, reason));
      });
    }
  } catch (err) {
    self._logger.error('Failed processing request: ' + err);
  }
};


SmartLightMqttClient.prototype.connect = function() {
  this._client = new mqtt.connect(this._url, this._opts);

  var self = this;

  this._client.on('connect', function () {
    self._logger.debug('connected to ' + self._url);
    self._isConnected = true;
    var cmdTopics = [];

    var regAllCmdTopics = function(topics) {
      return new Promise(function(resolve, reject) {
        self._client.subscribe(topics, {qos:2}, function(err, granted) {
          if ( err ) {
            self._logger.error('failed to subscribe to cmd topic: ' + err);
            reject(err);
          } else {
            for (var idx = 0; idx < granted.length; ++idx ) {
              self._logger.debug(format('subscribed to <{topic}> with QoS = {qos}',
                                      granted[idx]));
            }
            resolve();
          }
        });
      });
    };

    var startServer = function() {
      return new Promise(function(resolve, reject) {
        self._smartLight.start(function(err){
          if ( err ) {
            self._logger.error('failed to start SmartLight server: ' + err);
            reject(err);
          } else {
            self._logger.debug('SmartLight server started');
            resolve();
          }
        });
      });
    };

    var signalReady = function() {
      return new Promise(function(resolve, reject) {
        self._client.publish(self._opts.will.topic, 'online', {qos:2}, function() {
          self._logger.debug('signaled client is online');
          resolve();
        });
      });
    };

    for ( var name in self._bulbs ) {
      if ( self._bulbs.hasOwnProperty(name) ) {
        if ( typeof self._bulbs[name].cmdTopic === 'string' ) {
          cmdTopics.push(self._bulbs[name].cmdTopic);
        }
      }
    }
    
    return regAllCmdTopics(cmdTopics).then(startServer).then(signalReady);
  });


  this._client.on('offline', function() {
    self._logger.debug('client is offline');
    self._isConnected = false;
    self._smartLight.stop(function() {
      self._logger.debug('SmartLight controller stopped.');
    });
  });

  this._client.on('error', function(error) {
    self._logger.error('error received: ' + error);
  });

  this._client.on('reconnect', function(error) {
    self._logger.error('starting to reconnect client');
  });

  this._client.on('message', function(topic, msg, pkt) {
    self._onMessage(topic, msg, pkt);
  });

};

SmartLightMqttClient.prototype.close = function(callback) {
  if ( this._isConnected ) {
    this._client.end(false, callback);
  } else if ( typeof(callback) === 'function' ) {
    process.nextTick(callback);
  }
};



function execCliRequest(smartLight, argv) {
  var dev;
  //
  // Determine the device we're directing the request
  //
  if ( argv.device ) {
    dev = argv.device;
  }
  else {
    if ( argv.cfg && (typeof argv.cfg.bulbs === 'object') &&
        (typeof argv.bulb === 'string') &&
        (typeof argv.cfg.bulbs[argv.bulb] === 'object') &&
        (typeof argv.cfg.bulbs[argv.bulb].device === 'string') ) {
      dev = toDevice(argv.cfg.bulbs[argv.bulb].device);
      appLogger.debug('DeviceID: ', dev);
    }
    else {
      throw new Error('Unknown bulb or one not specified!');
    }
  }


  function displayResult(result) {
    return new Promise(function(resolve, reject) {
      console.log('Command complete.');
      console.log('Result: ' + result);
      resolve();
    });
  }

  function stopLight() {
    return new Promise(function(resolve, reject) {
      smartLight.stop(function() {
        resolve();
      });
    });
  }

  // Execute a command-line command
  return execCommand(smartLight, dev, argv, argv._)
    .then(displayResult)
    .then(stopLight)
    .catch(function(reason) {
      console.error('Command failed: ' + reason);
      stopLight().resolve();
    });
}


function main() {

  var logCfg = {
    appenders: [
      { type: "console" },
    ],
    replaceConsole: false
  };

  var smartLight;
  var af820Logger;
  var mqttClient;

	var argv = yargs
		.usage('Usage:\n' +
				'\nIn MQTT client (daemon) mode:\n' +
				'$0 -c|--cfg=<path_to_cfg> -m|--mqtt [-l|--log4js=<path_to_cfg>]' +
				'\n\nIn command-line mode:\n' +
				'$0 [-c|--cfg=<path_to_cfg>] [-l|--log4js=<path_to_cfg>] [-d|--device=<device_ID>] '+
				'[-b|--bulb=<bulb_name>] cmd [args..]')
		.options( {
			'c' : {
				alias: 'cfg',
				demand: false,
				default: path.join('.', 'cfg', 'smartlight.json'),
				describe: 'Path to the AF820 Smart Light configuration file.',
				nargs: 1,
				requiresArg: true
			},
      'l' : {
        alias: 'log4js',
        demand: false,
        default: path.join('.', 'cfg', 'log4js.json'),
        describe: 'Path to the LOG4JS configuration file.',
        nargs: 1,
        requiresArg: true
      },
			'm' : {
				alias: 'mqtt',
				demand: false,
				describe: 'Run as a daemon and processes commands ' +
						  'and provide responses to an MQTT broker.',
				boolean: true
			},
			'd' : {
				alias: 'device',
				demand: false,
				describe: 'Eight byte device identifier specified as a hex string.',
				string: true,
				nargs: 1,
				requiresArg: true
			},
      'b' : {
        alias: 'bulb',
        demand: false,
        describe: 'Bulb name as identified in configuration file.',
        string: true,
        nargs: 1,
        requiresArg: true
      },
      'v' : {
        alias: 'verbose',
        demand: false,
        describe: 'Enables verbose debug output.',
        boolean: false
      },
		})
		.count('verbose')
		.coerce('device', toDevice)
		.implies('mqtt', 'cfg')
		.coerce('cfg', function(cfgPath) {
			return JSON.parse(fs.readFileSync(cfgPath, 'utf-8'));
		})
    .coerce('log4js', function(cfgPath) {
      return JSON.parse(fs.readFileSync(cfgPath, 'utf-8'));
    })
    .command({
      command:'cancelRamp',
      desc: 'Cancels ramping the light up/down.\n',
    })
    .command({
      command:'checkWifi',
      desc: 'Check the status of the WiFi.\n',
    })
    .command({
      command:'checkStatus',
      desc: 'Check the status of the bulb.\n',
    })
    .command({
      command:'restartWifi',
      desc: 'Restarts WiFi and re-attaches to the configured AP.\n',
    })
    .command({
      command:'ramp',
      desc: 'Ramp up/down the light over a period of minutes to simulate ' +
            'sunrise/sunset.\n' +
            'Args: <up|down> <time>\n',
    })
    .command({
      command:'reset',
      desc: "Resets the smart light to its default (unpaired) settings.\n",
    })
    .command({
      command:'register',
      desc: "Register the bulb with a remote registration service.\n",
    })
    .command({
      command:'setRandom',
      desc: 'Enables or disables cycling through random colors.\n' +
            'Args: <true|false>\n',
    })
    .command({
      command:'setColor',
      desc: 'Sets the color of the bulb. Colors and lux range [0-255]. Time is ' +
            'in range [0-2000] msec.\n' +
            'Args: <red> <green> <blue> <white> <time> <lux>\n',
    })
    .command({
      command:'sleep',
      desc: 'Sets the time (in seconds) before the bulb turns off.\n' +
            'Args: <sleepTime>\n',
    })
    .command({
      command:'setWifi',
      desc: 'Sets the SSID and password for the AP.\nArgs: <ssid> <password>\n',
    })
    .command({
      command:'setBrightness',
      desc: 'Change the (rel)ative or (abs)olute brightness of the bulb ' +
            'over time [0-2000 msec].\n' +
            'Args: <rel|abs> <percent> <time>\n',
    })
    .command({
      command:'turnOn',
      desc: 'Turn on the bulb.\n',
    })
    .command({
      command:'turnOff',
      desc: 'Turn off the bulb.\n',
    })
    // Need to do more for the remaining commands
		.help()
		.alias('h', 'help')
		.strict()
		.global(['d', 'c', 'l', 'b', 'h', 'v'])
  		.argv;

	if ( argv.verbose ) {
	  appLogger.debug("Command-line arguments:\n" + util.inspect(argv));
	}


  // Configure the logger
  log4js.configure(argv.log4js || logCfg);
  
  af820Logger = log4js.getLogger('af820');
  smartLight = new AF820LightController(undefined, undefined, af820Logger);

  smartLight.start(function(err){
    if ( err ) {
      appLogger.error(err);
    } else {

      // Register a signal handler
      process.once('SIGINT', function() {
        if ( mqttClient ) {
          mqttClient.close(function() {
            console.log('MQTT connection closed.');
          });
        }
        if ( ramp ) {
          ramp.stop();
        }
        smartLight.stop(function() {
          console.log('Smart Light service stopped.');
        });
      });

      if ( argv.mqtt ) {
        // Create a MQTT client
        mqttClient = new SmartLightMqttClient(smartLight, argv);
        mqttClient.connect();
      }
      else {
        // Execute a command-line request
        execCliRequest(smartLight, argv);
      }
    }
  });
}


try {
  main();
}
catch ( e ) {
  appLogger.error(e.message);
}
